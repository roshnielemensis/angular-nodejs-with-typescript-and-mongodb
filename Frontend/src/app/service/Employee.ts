export class Employee {
    _id!: String;
    firstName!: String;
    lastName!: String;
    emailId!: String;
    dateofbirth! : Date;
    gender!: String;
    position!: String;
    salary!:Number;
    contactNo!:Number;
    address!:String;
    pincode!:String;

}