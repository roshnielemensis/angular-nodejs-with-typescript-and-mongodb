import { Component, OnInit,NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { CrudemployeeService } from 'src/app/service/crudemployee.service';
import { AbstractControl } from '@angular/forms';
import { FormBuilder ,FormControl,FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';


@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  employeeForm : FormGroup = new FormGroup({
     firstName : new FormControl(''),
     lastName :new FormControl(''),
     emailId : new FormControl(''),
     dateofbirth : new FormControl(''),
     gender : new FormControl(''),
     position: new FormControl(''),
     salary:new FormControl(''),
     contactNo:new FormControl(''),
     city: new FormControl(''),
     state:new FormControl(''),
     pincode : new FormControl('')
});

  submitted = false;
  constructor(
    public FormBuilder:FormBuilder,
    private router:Router,
    private ngZone :NgZone,
    private crudemployeeService :CrudemployeeService

  ) {
    this.employeeForm = this.FormBuilder.group({
      firstName:['',[Validators.required,Validators.pattern('^[a-zA-Z \-\']+')]],
      // Validators.pattern('^[a-zA-Z \-\']+')
      lastName:['',[Validators.required,Validators.pattern('^[a-zA-Z \-\']+')]],
      emailId:['',[Validators.required, Validators.email]],
      dateofbirth :['',[Validators.required,Validators.pattern(/^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/)]],
      gender:['',Validators.required],
      position:['',Validators.required],
      salary: ['',Validators.required],
      contactNo:['',[
        Validators.required,
        Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")
      ]],
      city:['',Validators.required],
      state:['',Validators.required],
      pincode :['',Validators.required]
    })
   }

  ngOnInit(): void {}
  get f(): { [key: string]: AbstractControl } {
    return this.employeeForm.controls;
  }
  onSubmit():any{
    this.submitted = true;
    this.crudemployeeService.AddEmployee(this.employeeForm.value)
    .subscribe(()=> {
      console.log('Data added Successfully!')
      this.ngZone.run(()=> this.router.navigateByUrl('/employee-list'))
      if(this.employeeForm.invalid){
        return
      }
      console.log(JSON.stringify(this.employeeForm.value,null,2));
    },(err) => {
      console.log(err);
    });
  }

 }
