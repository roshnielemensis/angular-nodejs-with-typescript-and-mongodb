import { Component, OnInit, NgZone } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CrudemployeeService } from './../../service/crudemployee.service';
import { FormGroup, FormBuilder,FormControl } from "@angular/forms";
import { AbstractControl } from '@angular/forms';
import { Validators } from '@angular/forms';
@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {
//   updateForm : FormGroup = new FormGroup({
//     firstName : new FormControl(''),
//     lastName :new FormControl(''),
//     emailId : new FormControl(''),
//     dateofbirth : new FormControl(''),
//     gender : new FormControl(''),
//     position: new FormControl(''),
//     salary:new FormControl(''),
//     contactNo:new FormControl(''),
//     city: new FormControl(''),
//     state:new FormControl(''),
//     pincode : new FormControl('')
// });
  getId: any;

  updateForm: FormGroup;
  
  constructor(
    public formBuilder: FormBuilder,
    private router: Router,
    private ngZone: NgZone,
    private activatedRoute: ActivatedRoute,
    private crudemployeeService: CrudemployeeService
  ) {
    this.getId = this.activatedRoute.snapshot.paramMap.get('id');
    this.crudemployeeService.GetEmployee(this.getId).subscribe(res => {
      this.updateForm.setValue({
        firstName:res['firstName'],
        lastName: res['lastName'],
        emailId: res['emailId'],
        position: res['position'],
        salary:res['salary'],
        contactNo:res['contactNo'],
        city:res['city'],
        state:res['state'],
        pincode:res['pincode']
        
      });
      
    });
    this.updateForm = this.formBuilder.group({
      firstName:[''],
      lastName: [''],
      emailId: [''],
      position: [''],
      salary:[''],
      contactNo:[''],
      city:[''],
      state:[''],
      pincode:['']
    })
  }
  ngOnInit() { 
    {
      this.updateForm = this.formBuilder.group({
        firstName: new FormControl('',[Validators.required,Validators.pattern('^[a-zA-Z \-\']+')]),
        lastName:new FormControl('',[Validators.required,Validators.pattern('^[a-zA-Z \-\']+')]),
        emailId:new FormControl('',[Validators.required, Validators.email]),
        position:new FormControl('',[Validators.required]),
        salary: new FormControl('',[Validators.required]),
        contactNo:new FormControl('',[
          Validators.required,
          Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")
        ]),
        city:new FormControl('',[Validators.required]),
        state:new FormControl('',[Validators.required]),
        pincode :new FormControl('',[Validators.required,Validators.pattern("[0-9 ]{6}")])
      })
    }
  }
  get f(): { [key: string]: AbstractControl } {
    return this.updateForm.controls;
  }
  onUpdate(): any {
    this.crudemployeeService.updateEmployee(this.getId, this.updateForm.value)
    .subscribe(() => {
        console.log('Data updated successfully!')
        this.ngZone.run(() => this.router.navigateByUrl('/employee-list'))
      }, (err) => {
        console.log(err);
    });
  }
}