import { Component, OnInit } from '@angular/core';
import { CrudemployeeService } from 'src/app/service/crudemployee.service';
import { HttpClient } from '@angular/common/http';


interface Employee {
  firstName: String;
  lastName: String;
  emailId:String;
  dateofbirth :Date;
  gender:String;
  position:String;
  salary: Number;
  contactNo:Number;
  address:String;
  pincode :String;
}

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  
  Employees:any = [];
  searchTerm ='';
  employees:Employee[]=[];
  term ='';

  page = 1;
  count = 0;
  pageSize = 10;
  // pageSizes = [3, 6, 9];
  
  // searchText: any;
  constructor(private crudemployeeService:CrudemployeeService,
    private http:HttpClient) { }

  ngOnInit(): void {
    this.crudemployeeService.getEmployee().subscribe(res => {
      console.log(res)
      this.Employees = res;
  });

}
handlePageChange(event: number): void {
  this.page = event;
  
}
delete(id:any, i:any) {
  console.log(id);
  if(window.confirm('Do you want to go ahead?')) {
    this.crudemployeeService.deleteEmployee(id).subscribe((res) => {
      this.Employees.splice(i, 1);
    })
  }
 }
}
