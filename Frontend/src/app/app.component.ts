import { HttpClient } from '@angular/common/http';
import { Component,OnInit } from '@angular/core';

interface Employee {
  firstName: String;
  lastName: String;
  emailId:String;
  dateofbirth :Date;
  gender:String;
  position:String;
  salary: Number;
  contactNo:Number;
  city:String;
  state:String;
  pincode :String;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'CRUD_Employee';
  searchTerm ='';
  employees:Employee[]=[];
  term ='';

  constructor(private http:HttpClient){}
  ngOnInit(): void {
  }
}