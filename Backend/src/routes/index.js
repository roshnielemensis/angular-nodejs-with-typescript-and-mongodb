"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const employee_1 = require("./employee");
const routes = (0, express_1.Router)();
routes.use('/api', employee_1.emprouter);
exports.default = routes;
