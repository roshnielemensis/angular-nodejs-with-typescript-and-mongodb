

import { Router } from 'express';
import {emprouter} from './employee';

const routes = Router();

routes.use('/api', emprouter);

export default routes;