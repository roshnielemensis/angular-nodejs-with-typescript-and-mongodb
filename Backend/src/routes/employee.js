"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.emprouter = void 0;
const express_1 = require("express");
const Employee_1 = require("../models/Employee");
const employeeRoute = (0, express_1.Router)({ mergeParams: true });
exports.emprouter = employeeRoute;
//Get Employee
employeeRoute.get('/getEmployee', (req, res) => {
    Employee_1.Employee.find((error, data) => {
        if (error) {
            return next(error);
        }
        else {
            res.json(data);
        }
    });
});
//Add Employee
employeeRoute.post('/add-employee', (req, res) => {
    Employee_1.Employee.create(req.body, (error, data) => {
        if (error) {
            return next(error);
        }
        else {
            res.json(data);
        }
    });
});
employeeRoute.get('/read-employee/:id', (req, res) => {
    Employee_1.Employee.findById(req.params.id, (error, data) => {
        if (error) {
            return next(error);
        }
        else {
            res.json(data);
        }
    });
});
//Update Employee
employeeRoute.put('/update-employee/:id', (req, res) => {
    Employee_1.Employee.findByIdAndUpdate(req.params.id, {
        $set: req.body
    }, (error, data) => {
        if (error) {
            return next(error);
            console.log(error);
        }
        else {
            res.json(data);
            console.log('Employee updated successfully!');
        }
    });
});
//Delete Employee
employeeRoute.delete('/delete-employee/:id', (req, res) => {
    Employee_1.Employee.findByIdAndRemove(req.params.id, (error, data) => {
        if (error) {
            return next(error);
        }
        else {
            res.status(200).json({
                msg: data
            });
        }
    });
});
function next(error) {
    throw new Error('Function not implemented.');
}
