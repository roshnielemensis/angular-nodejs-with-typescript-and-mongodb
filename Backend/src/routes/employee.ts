import express,{NextFunction, Request,Response, Router} from 'express';
import { Employee } from '../models/Employee';

const employeeRoute = Router({mergeParams: true})

//Get Employee
employeeRoute.get('/getEmployee',(req:Request,res:Response)=> {
    Employee.find((error,data)=> {
        if(error){
            return next(error)
        }else{
            res.json(data)
        }

    })
})

//Add Employee
employeeRoute.post('/add-employee',(req:Request,res:Response)=> {
    Employee.create(req.body,(error: NativeError,data: any) => {
        if(error){
            return next(error)
        }else{
            res.json(data)
        }
    })
});
employeeRoute.get('/read-employee/:id',(req:Request,res:Response)=> {
    Employee.findById(req.params.id,(error: NativeError,data: any)=>{
        if(error){
            return next(error)
        }else{
            res.json(data)
        }
    })
});

//Update Employee
employeeRoute.put('/update-employee/:id',(req:Request,res:Response)=> {
    Employee.findByIdAndUpdate(req.params.id,{
        $set:req.body
    },(error: NativeError,data: any)=> {
        if (error) {
            return next(error);
            console.log(error)
          } else {
            res.json(data)
            console.log('Employee updated successfully!')
          }
    })
})

//Delete Employee
employeeRoute.delete('/delete-employee/:id',(req:Request,res:Response)=> {
    Employee.findByIdAndRemove(req.params.id, (error: NativeError, data: any) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: data
      })
    }
  })
})
    

export{ employeeRoute as emprouter}

function next(error: NativeError): void {
    throw new Error('Function not implemented.');
}
