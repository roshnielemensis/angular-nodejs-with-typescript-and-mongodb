import { model, Schema, Document} from 'mongoose';

interface IEmployee extends Document{
  firstName:string;
  lastname:string;
  emailId:string;
  dateofbirth:Date;
  gender:string;
  position:string;
  salary:string;
  contactNo:number;
  city:string;
  state:string;
  pincode:number;
}

const employeeschema = new Schema({
  firstName:{ type: String},
  lastName:{type: String},
  emailId:{type: String},
  dateofbirth :{type:Date},
  gender: {type:String },
  position:{type:String},
  salary: {type:Number},
  contactNo:{type:Number},
  city:{type:String},
  state:{type:String},
  pincode :{type:Number}
})

const Employee = model<IEmployee>('Employee', employeeschema, 'employees');

export{Employee, IEmployee}
