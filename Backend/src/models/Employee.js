"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Employee = void 0;
const mongoose_1 = require("mongoose");
const employeeschema = new mongoose_1.Schema({
    firstName: { type: String },
    lastName: { type: String },
    emailId: { type: String },
    dateofbirth: { type: Date },
    gender: { type: String },
    position: { type: String },
    salary: { type: Number },
    contactNo: { type: Number },
    city: { type: String },
    state: { type: String },
    pincode: { type: Number }
});
const Employee = (0, mongoose_1.model)('Employee', employeeschema, 'employees');
exports.Employee = Employee;
