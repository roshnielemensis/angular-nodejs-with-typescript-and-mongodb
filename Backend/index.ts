import express from 'express';
import mongoose from 'mongoose';
import routes from '../Backend/src/routes/index';

const cors = require('cors')


const app = express()
const PORT = 5001

var db = "mongodb://localhost:27017/CRUDemployee";
mongoose.connect(db,{dbName : 'CRUDemployee' , autoIndex: true, autoCreate: true})
const conSuccess = mongoose.connection
conSuccess.once('open', _ => {
  console.log('Database connected:', db)
})

app.use(cors())

app.listen(PORT,()=> {
    console.log(`server is listening on port ${PORT}`)
});
app.use(express.json());
app.use(routes);