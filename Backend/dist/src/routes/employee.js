"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.emprouter = void 0;
const express_1 = require("express");
const Employee_1 = require("models/Employee");
const employeeRoute = (0, express_1.Router)({ mergeParams: true });
exports.emprouter = employeeRoute;
employeeRoute.get('/getEmployee', (res) => {
    Employee_1.Employee.find((data, error) => {
        if (data) {
            console.log(data);
        }
        else {
            console.log(error);
        }
        console.log(res);
        return res.send('All employees');
    });
});
employeeRoute.post('/addEmployee', (req, res) => {
    return res.send('new employee created');
});
//# sourceMappingURL=employee.js.map