"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const index_1 = __importDefault(require("../Backend/src/routes/index"));
const cors = require('cors');
const app = (0, express_1.default)();
const PORT = 5001;
var db = "mongodb://localhost:27017/CRUDemployee";
mongoose_1.default.connect(db, { dbName: 'CRUDemployee', autoIndex: true, autoCreate: true });
const conSuccess = mongoose_1.default.connection;
conSuccess.once('open', _ => {
    console.log('Database connected:', db);
});
app.use(cors());
app.listen(PORT, () => {
    console.log(`server is listening on port ${PORT}`);
});
app.use(express_1.default.json());
app.use(index_1.default);
